#!/usr/bin/env bash

[[ -n "$1" ]] && sleep $1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

echo "server building..."
gleam build
echo "server starting..."
gleam run
echo "server exited!"
