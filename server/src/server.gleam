import client/view
import gleam/dict
import gleam/erlang/os
import gleam/erlang/process
import gleam/io
import gleam/option.{None}
import gleam/pgo
import illustrious/server
import lustre/attribute
import lustre/element
import lustre/element/html
import mist
import server/db
import server/dotenv
import server/router
import server/server_state
import server/web
import shared/app_state.{AppState}
import simplifile
import snag
import wisp

type StartupError {
  MissingEnvVar(String)
}

pub fn main() {
  case simplifile.is_file(".env") {
    Ok(True) -> {
      io.println("Loading .env")
      dotenv.config()
    }
    _ -> Nil
  }

  wisp.configure_logger()
  let secret_key_base = wisp.random_string(64)
  let assert Ok(preshared_key) = os.get_env("QE_PRESHARED_KEY")
  let assert Ok(_) = case preshared_key {
    "" -> Error(MissingEnvVar("QE_PRESHARED_KEY"))
    _ -> Ok(Nil)
  }

  let app =
    server.build_app(
      server.ServerAppBuilder(
        view: view.root,
        default_model: AppState(dict.new(), None),
        updater: app_state.updater,
        decoder: app_state.app_decoder,
        initializer: server_state.initializer,
        loader: server_state.loader,
        error_serializer: snag.pretty_print,
        html_headers: fn(_, _) {
          element.fragment([
            html.title([], "Gleam Quote Explorer"),
            html.link([
              attribute.rel("icon"),
              attribute.href("/static/favicon.svg"),
              attribute.type_("image/svg+xml"),
            ]),
          ])
        },
      ),
    )

  let ctx =
    web.Context(
      static_directory: static_directory(),
      db: db_connection(),
      key: preshared_key,
      app: app,
    )

  let assert Ok(_) = db.init(ctx.db)

  wisp.mist_handler(router.handle_request(_, ctx), secret_key_base)
  |> mist.new
  |> mist.port(8000)
  |> listen()

  process.sleep_forever()
}

fn static_directory() -> String {
  let assert Ok(priv_directory) = wisp.priv_directory("server")
  priv_directory <> "/static"
}

fn db_connection() {
  let assert Ok(pg_uri) = os.get_env("PG_URI")
  let assert Ok(pg_config) = pgo.url_config(pg_uri)
  let ip_version = case os.get_env("PG_USE_IPV6") {
    Ok("TRUE") -> pgo.Ipv6
    _ -> pgo.Ipv4
  }
  pgo.connect(pgo.Config(..pg_config, ip_version: ip_version))
}

fn listen() {
  let cert_file = os.get_env("CERT_FILE")
  let key_file = os.get_env("KEY_FILE")

  case cert_file, key_file {
    Ok(cert), Ok(key) -> fn(builder) {
      let assert Ok(_) = mist.start_https(builder, cert, key)
      Nil
    }
    _, _ -> fn(builder) {
      let assert Ok(_) = mist.start_http(builder)
      Nil
    }
  }
}
