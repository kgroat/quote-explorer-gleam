import birl
import gleam/int
import gleam/json
import gleam/result
import illustrious.{type Path}
import server/db/db_quote
import server/web
import shared/app_state/state_quote.{type Quote, Quote}
import snag

fn quote_to_json(quote: Quote) {
  let Quote(id, quote, person, created_at) = quote

  json.object([
    #("id", json.int(id)),
    #("quote", json.string(quote)),
    #("person", json.string(person)),
    #("created_at", json.string(birl.to_iso8601(created_at))),
  ])
}

pub fn initializer(path: Path, ctx: web.Context) {
  case path {
    [] -> Ok(json.null())
    ["quote"] -> {
      let skip = 0
      let take = 10

      db_quote.list_quotes(ctx.db, skip, take)
      |> result.map(json.array(_, quote_to_json))
      |> result.map(fn(latest_quotes) {
        json.object([
          #("type", json.string("load_state")),
          #("quotes_by_id", json.object([])),
          #("latest_quotes", latest_quotes),
        ])
      })
    }
    ["quote", "random"] -> {
      let id = db_quote.get_random_id(ctx.db)

      db_quote.get_by_id(ctx.db, id)
      |> result.map(fn(quote) {
        let json = quote_to_json(quote)
        let id = quote.id
        let id_str = int.to_string(id)

        json.array(
          [
            json.object([
              #("type", json.string("load_state")),
              #("quotes_by_id", json.object([#(id_str, json)])),
            ]),
            json.object([
              #("type", json.string("redirect")),
              #("location", json.string("/quote/" <> id_str)),
            ]),
          ],
          fn(json) { json },
        )
      })
    }
    ["quote", id_str] -> {
      case int.parse(id_str) {
        Error(_) -> snag.error("400")
        Ok(id) -> {
          db_quote.get_by_id(ctx.db, id)
          |> result.map(fn(quote) {
            let json = quote_to_json(quote)
            let id = quote.id
            let id_str = int.to_string(id)

            json.object([
              #("type", json.string("load_state")),
              #("quotes_by_id", json.object([#(id_str, json)])),
            ])
          })
        }
      }
    }
    _ -> snag.error("404")
  }
}

pub fn loader(path: Path, ctx: web.Context) {
  case path {
    [] -> Ok(json.null())
    ["quote"] -> {
      let skip = 0
      let take = 10

      db_quote.list_quotes(ctx.db, skip, take)
      |> result.map(json.array(_, quote_to_json))
      |> result.map(fn(latest_quotes) {
        json.object([
          #("type", json.string("set_latest_quotes")),
          #("quotes", latest_quotes),
        ])
      })
    }
    ["quote", "random"] -> {
      let id = db_quote.get_random_id(ctx.db)

      db_quote.get_by_id(ctx.db, id)
      |> result.map(fn(quote) {
        let json = quote_to_json(quote)
        let id = quote.id
        let id_str = int.to_string(id)

        json.array(
          [
            json.object([#("type", json.string("add_quote")), #("quote", json)]),
            json.object([
              #("type", json.string("redirect")),
              #("location", json.string("/quote/" <> id_str)),
            ]),
          ],
          fn(json) { json },
        )
      })
    }
    ["quote", id_str] -> {
      case int.parse(id_str) {
        Error(_) -> snag.error("400")
        Ok(id) -> {
          db_quote.get_by_id(ctx.db, id)
          |> result.map(fn(quote) {
            let json = quote_to_json(quote)

            json.object([#("type", json.string("add_quote")), #("quote", json)])
          })
        }
      }
    }
    _ -> snag.error("404")
  }
}
