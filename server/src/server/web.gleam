import gleam/http
import gleam/http/request
import gleam/http/response.{Response}
import gleam/option.{type Option, Some}
import gleam/pgo
import illustrious/server.{type ServerApp}
import shared/app_state.{type AppAction, type AppState}
import snag.{type Snag}
import wisp

pub type Context {
  Context(
    static_directory: String,
    db: pgo.Connection,
    key: String,
    app: ServerApp(Context, AppState, AppAction, Snag),
  )
}

pub fn middleware(
  req: wisp.Request,
  ctx: Context,
  handle_request: fn(wisp.Request) -> wisp.Response,
) -> wisp.Response {
  let req = wisp.method_override(req)
  use <- wisp.log_request(req)
  use <- wisp.rescue_crashes
  use req <- wisp.handle_head(req)
  use <- wisp.serve_static(req, under: "/static", from: ctx.static_directory)
  use <- serve_favicon(req, ctx)

  handle_request(req)
}

pub fn check_preshared_key(
  req: wisp.Request,
  ctx: Context,
  handler: fn() -> Option(wisp.Response),
) {
  let bearer_token = "Bearer " <> ctx.key
  case request.get_header(req, "Authorization") {
    Ok(value) if value == bearer_token -> handler()
    _ -> Some(Response(401, [], wisp.Empty))
  }
}

fn serve_favicon(
  req: wisp.Request,
  ctx: Context,
  next: fn() -> wisp.Response,
) -> wisp.Response {
  case req.method, req.path {
    http.Get, "/favicon.ico" -> {
      response.new(200)
      |> response.set_header("content-type", "image/x-icon")
      |> response.set_body(wisp.File(ctx.static_directory <> "/favicon.ico"))
    }
    _, _ -> next()
  }
}
