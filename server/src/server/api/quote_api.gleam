import birl
import gleam/dict
import gleam/dynamic
import gleam/http
import gleam/int
import gleam/json.{type Json}
import gleam/option.{type Option, None, Some}
import gleam/result
import server/db/db_quote.{type QuoteInput, QuoteInput}
import server/errors
import server/web.{type Context}
import shared/app_state/state_quote.{type Quote, Quote}
import snag
import wisp.{type Request, type Response}

fn quote_input_decoder() {
  dynamic.decode2(
    QuoteInput,
    dynamic.field("quote", dynamic.string),
    dynamic.field("person", dynamic.string),
  )
}

fn json_to_quote_input(json: String) {
  json.decode(json, quote_input_decoder())
  |> result.replace_error(snag.new("Failed to decode quote input from json"))
}

fn quote_to_json(quote: Quote) {
  let Quote(id, quote, person, created_at) = quote

  json.object([
    #("id", json.int(id)),
    #("quote", json.string(quote)),
    #("person", json.string(person)),
    #("created_at", json.string(birl.to_iso8601(created_at))),
  ])
}

fn to_json_response(json: Json) {
  wisp.json_response(json.to_string_builder(json), 200)
}

pub fn quote_router(
  req: Request,
  ctx: Context,
  route: List(String),
) -> Option(Response) {
  case route {
    [] -> {
      case req.method {
        http.Get -> {
          let query = dict.from_list(wisp.get_query(req))
          let skip =
            dict.get(query, "skip")
            |> result.try(int.parse)
            |> result.unwrap(0)
          let take =
            dict.get(query, "take")
            |> result.try(int.parse)
            |> result.unwrap(10)
            |> int.clamp(min: 1, max: 25)

          let response =
            db_quote.list_quotes(ctx.db, skip, take)
            |> result.map(json.array(_, quote_to_json))
            |> result.map(to_json_response)
            |> errors.handle_error

          Some(response)
        }
        http.Post -> {
          use <- web.check_preshared_key(req, ctx)
          let response = {
            use body <- wisp.require_string_body(req)
            json_to_quote_input(body)
            |> result.try(db_quote.create(ctx.db, _))
            |> result.map(quote_to_json)
            |> result.map(to_json_response)
            |> errors.handle_error
          }
          Some(response)
        }
        _ -> Some(wisp.method_not_allowed([http.Get, http.Post]))
      }
    }
    ["random_id"] -> {
      case req.method {
        http.Get -> {
          Some(wisp.json_response(
            json.to_string_builder(json.int(db_quote.get_random_id(ctx.db))),
            200,
          ))
        }
        _ -> Some(wisp.method_not_allowed([http.Get]))
      }
    }
    [id_str] -> {
      use id <- option.then(option.from_result(int.parse(id_str)))

      case req.method {
        http.Get -> {
          case db_quote.get_by_id(ctx.db, id) {
            Ok(quote) -> {
              Some(wisp.json_response(
                json.to_string_builder(quote_to_json(quote)),
                200,
              ))
            }
            Error(snag.Snag(desc, _)) if desc == errors.not_found -> {
              Some(wisp.not_found())
            }
            _ -> Some(wisp.internal_server_error())
          }
        }
        http.Delete -> {
          use <- web.check_preshared_key(req, ctx)

          let response =
            db_quote.delete_by_id(ctx.db, id)
            |> result.map(fn(_) { wisp.no_content() })
            |> errors.handle_error

          Some(response)
        }
        _ -> Some(wisp.method_not_allowed([http.Get, http.Delete]))
      }
    }
    _ -> None
  }
}
