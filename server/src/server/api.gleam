import gleam/option.{type Option, None}

import server/api/quote_api.{quote_router}
import server/web.{type Context}
import wisp.{type Request, type Response}

pub fn api_router(
  req: Request,
  ctx: Context,
  route: List(String),
) -> Option(Response) {
  case route {
    ["quote", ..sub_route] -> quote_router(req, ctx, sub_route)
    _ -> None
  }
}
