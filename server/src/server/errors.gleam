import snag
import wisp

pub const not_found = "404 not found"

pub fn handle_error(error: snag.Result(wisp.Response)) {
  case error {
    Ok(response) -> response
    Error(snag) ->
      wisp.internal_server_error() |> wisp.string_body(snag.pretty_print(snag))
  }
}
