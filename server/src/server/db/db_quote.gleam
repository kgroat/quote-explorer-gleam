import birl
import gleam/dynamic
import gleam/float
import gleam/int
import gleam/io
import gleam/pgo.{type Connection, Returned}
import gleam/result
import server/errors
import shared/app_state/state_quote.{type Quote, Quote}
import snag

pub type QuoteInput {
  QuoteInput(quote: String, person: String)
}

pub fn init_table(db: Connection) {
  pgo.execute(
    "
    create table if not exists
    quote (
      id serial primary key,
      quote text unique not null,
      person text not null,
      created_at timestamp not null default now()
    )
  ",
    db,
    [],
    dynamic.dynamic,
  )
}

const select_quote = "select id, quote, person, created_at from quote "

fn timestamp_decoder(input: dynamic.Dynamic) {
  let date_decoder = dynamic.tuple3(dynamic.int, dynamic.int, dynamic.int)
  let time_decoder = dynamic.tuple3(dynamic.int, dynamic.int, dynamic.float)
  use foo <- result.map(dynamic.tuple2(date_decoder, time_decoder)(input))
  let #(#(year, month, day), #(hour, minute, second_float)) = foo
  let second = float.truncate(second_float)
  let millisecond = float.truncate(second_float *. 1000.0)
  birl.now()
  |> birl.set_day(birl.Day(year, month, day))
  |> birl.set_time_of_day(birl.TimeOfDay(hour, minute, second, millisecond))
}

pub fn get_random_id(db: Connection) -> Int {
  let skip_result = {
    use returned <- result.map(pgo.execute(
      "select count(*) from quote",
      db,
      [],
      dynamic.element(0, dynamic.int),
    ))
    let assert pgo.Returned(count: _, rows: [count]) = returned
    count
  }

  let skip = case skip_result {
    Ok(count) -> int.random(count)
    _ -> 0
  }

  case
    pgo.execute(
      "select id from quote limit 1 offset $1",
      db,
      [pgo.int(skip)],
      dynamic.element(0, dynamic.int),
    )
  {
    Ok(Returned(_, [id])) -> id
    _ -> 0
  }
}

pub fn get_by_id(db: Connection, id: Int) {
  case
    pgo.execute(
      select_quote <> "where id = $1",
      db,
      [pgo.int(id)],
      quote_decoder,
    )
  {
    Ok(Returned(_, [quote])) -> Ok(quote)
    _ -> snag.error(errors.not_found)
  }
}

pub fn create(db: Connection, input: QuoteInput) {
  case
    pgo.execute(
      "insert into quote(quote, person) values($1, $2) returning id, quote, person, created_at",
      db,
      [pgo.text(input.quote), pgo.text(input.person)],
      quote_decoder,
    )
  {
    Ok(Returned(_, [id])) -> Ok(id)
    error -> {
      io.debug(error)
      snag.error("failed to create a quote")
    }
  }
}

pub fn delete_by_id(db: Connection, id: Int) {
  pgo.execute("delete quote where id = $1", db, [pgo.int(id)], dynamic.dynamic)
  |> result.replace_error(snag.new("failed to delete quote"))
}

fn quote_decoder(dynamic) {
  use result <- result.map(dynamic.tuple4(
    dynamic.int,
    dynamic.string,
    dynamic.string,
    timestamp_decoder,
  )(dynamic))
  let #(id, quote, person, date) = result
  Quote(id, quote, person, date)
}

pub fn list_quotes(db: Connection, skip: Int, limit: Int) {
  case
    pgo.execute(
      select_quote <> "order by created_at desc limit $1 offset $2",
      db,
      [pgo.int(limit), pgo.int(skip)],
      quote_decoder,
    )
  {
    Ok(Returned(_, quotes)) -> Ok(quotes)
    _ -> Ok([])
  }
}
