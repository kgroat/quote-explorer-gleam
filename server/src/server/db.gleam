import gleam/pgo.{type Connection}
import server/db/db_quote

pub fn init(db: Connection) {
  db_quote.init_table(db)
}
