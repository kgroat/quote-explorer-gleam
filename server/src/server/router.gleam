import gleam/json
import gleam/option.{Some}
import illustrious/server.{Data, NoResult, ServerError, View}
import lustre/element
import server/api
import server/web.{type Context}
import wisp.{type Request, type Response}

pub fn handle_request(req: Request, ctx: Context) -> Response {
  use req <- web.middleware(req, ctx)

  case wisp.path_segments(req) {
    ["api", ..route] -> {
      let assert Some(response) = api.api_router(req, ctx, route)
      response
    }
    path -> {
      use body <- wisp.require_string_body(req)
      case server.handle_request(ctx.app, req.method, path, body, ctx) {
        View(element) -> {
          wisp.html_response(element.to_string_builder(element), 200)
        }
        Data(json) -> {
          wisp.json_response(json.to_string_builder(json), 200)
        }
        ServerError(error_str) -> {
          wisp.internal_server_error()
          |> wisp.string_body(error_str)
        }
        NoResult -> {
          wisp.not_found()
        }
      }
    }
  }
}
