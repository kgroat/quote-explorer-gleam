import birl

pub type Quote {
  Quote(id: Int, quote: String, person: String, created_at: birl.Time)
}
