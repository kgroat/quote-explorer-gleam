import birl
import gleam/dict.{type Dict}
import gleam/dynamic.{type Dynamic}
import gleam/int
import gleam/option.{type Option, Some}
import gleam/result
import illustrious/client
import illustrious/state
import lustre/effect
import shared/app_state/state_quote.{type Quote, Quote}

pub type AppState {
  AppState(quotes_by_id: Dict(Int, Quote), latest_quotes: Option(List(Quote)))
}

pub type AppAction {
  SetState(AppState)
  UpsertQuote(Quote)
  FetchRandomQuoteId
  RandomQuoteId(Int)
  SetLatestQuotes(List(Quote))
  Noop
}

fn timestamp_decoder(input: dynamic.Dynamic) {
  dynamic.string(input)
  |> result.try(fn(str) { birl.parse(str) |> result.replace_error([]) })
}

pub fn quote_decoder(dyn) {
  dynamic.decode4(
    Quote,
    dynamic.field("id", dynamic.int),
    dynamic.field("quote", dynamic.string),
    dynamic.field("person", dynamic.string),
    dynamic.field("created_at", timestamp_decoder),
  )(dyn)
}

fn decode_id(dyn: Dynamic) {
  dynamic.string(dyn)
  |> result.try(fn(str_id) { int.parse(str_id) |> result.replace_error([]) })
}

pub fn app_decoder(dyn: Dynamic) {
  dynamic.optional(dynamic.field("type", dynamic.string))(dyn)
  |> result.map(fn(action_type) {
    case action_type {
      Some("load_state") -> {
        let assert Ok(state) =
          dynamic.decode2(
            AppState,
            dynamic.field(
              "quotes_by_id",
              dynamic.dict(decode_id, quote_decoder),
            ),
            dynamic.optional_field("latest_quotes", dynamic.list(quote_decoder)),
          )(dyn)

        state.Perform(SetState(state))
      }
      Some("redirect") -> {
        let assert Ok(location) = dynamic.field("location", dynamic.string)(dyn)
        client.redirect(location)
      }
      Some("add_quote") -> {
        let assert Ok(quote) = dynamic.field("quote", quote_decoder)(dyn)
        client.perform(UpsertQuote(quote))
      }
      Some("set_latest_quotes") -> {
        let assert Ok(quotes) =
          dynamic.field("quotes", dynamic.list(quote_decoder))(dyn)
        client.perform(SetLatestQuotes(quotes))
      }
      _ -> state.Noop
    }
  })
}

pub fn updater(state: AppState, action: AppAction) {
  case action {
    SetState(state) -> #(state, effect.none())
    UpsertQuote(quote) -> {
      let new_quotes_by_id = dict.insert(state.quotes_by_id, quote.id, quote)
      #(AppState(..state, quotes_by_id: new_quotes_by_id), effect.none())
    }
    FetchRandomQuoteId -> {
      #(state, effect.none())
    }
    RandomQuoteId(id) -> {
      #(
        state,
        effect.from(fn(dispatch) {
          dispatch(client.redirect("/quote/" <> int.to_string(id)))
        }),
      )
    }
    SetLatestQuotes(quotes) -> {
      #(AppState(..state, latest_quotes: Some(quotes)), effect.none())
    }
    Noop -> #(state, effect.none())
  }
}
