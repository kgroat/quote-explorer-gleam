#!/usr/bin/env bash

[[ -n "$1" ]] && sleep $1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR

gleam run -m lustre/dev build --tailwind-entry=index.css --outdir=../server/priv/static
