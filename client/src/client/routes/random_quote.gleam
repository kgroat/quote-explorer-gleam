import lustre/attribute
import lustre/element
import lustre/element/html

pub fn random_quote() {
  html.main([attribute.class("flex flex-col items-center justify-center")], [
    html.h1([], [element.text("Loading...")]),
  ])
}
