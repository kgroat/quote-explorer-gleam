import lustre/attribute
import lustre/element
import lustre/element/html

pub fn not_found() {
  html.main(
    [
      attribute.class("flex flex-col items-center justify-center"),
      attribute.style([#("gap", "24px")]),
    ],
    [
      html.h1([], [element.text("404")]),
      html.h2([], [element.text("Page not found")]),
    ],
  )
}
