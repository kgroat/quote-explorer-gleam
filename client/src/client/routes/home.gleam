import lustre/attribute
import lustre/element
import lustre/element/html

pub fn home() {
  html.main([attribute.class("flex flex-col items-center justify-center")], [
    html.h1([], [element.text("Home")]),
  ])
}
