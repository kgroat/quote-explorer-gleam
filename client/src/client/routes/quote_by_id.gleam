import gleam/option.{type Option, Some}
import lustre/attribute
import lustre/element
import lustre/element/html
import shared/app_state/state_quote.{type Quote}

pub fn quote_by_id(maybe_quote: Option(Quote)) {
  case maybe_quote {
    Some(quote) ->
      html.main(
        [
          attribute.class("flex flex-col items-center justify-center"),
          attribute.style([#("gap", "24px")]),
        ],
        [
          html.h1([], [element.text(quote.quote)]),
          html.h2([], [element.text(quote.person)]),
        ],
      )
    _ ->
      html.main([attribute.class("flex flex-col items-center justify-center")], [
        html.h1([], [element.text("Loading...")]),
      ])
  }
}
