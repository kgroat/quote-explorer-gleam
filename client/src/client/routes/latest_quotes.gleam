import gleam/int
import gleam/list
import gleam/option.{type Option, Some}
import illustrious/link.{link}
import lustre/attribute
import lustre/element
import lustre/element/html
import shared/app_state/state_quote.{type Quote}

pub fn latest_quotes(quotes: Option(List(Quote))) {
  case quotes {
    Some(quotes) -> {
      html.main([attribute.class("flex flex-col items-center justify-center")], [
        html.div(
          [
            attribute.style([
              #("display", "flex"),
              #("flex-direction", "column"),
            ]),
          ],
          list.map(quotes, fn(quote) {
            link("/quote/" <> int.to_string(quote.id), [], [
              element.text(quote.quote),
            ])
          }),
        ),
      ])
    }
    _ ->
      html.main([attribute.class("flex flex-col items-center justify-center")], [
        html.h1([], [element.text("Loading...")]),
      ])
  }
}
