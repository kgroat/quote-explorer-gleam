import client/routes/home.{home}
import client/routes/latest_quotes.{latest_quotes}
import client/routes/not_found.{not_found}
import client/routes/quote_by_id.{quote_by_id}
import client/routes/random_quote.{random_quote}
import gleam/dict
import gleam/int
import gleam/option.{Some}
import gleam/result
import illustrious/link.{link}
import lustre/attribute
import lustre/element
import lustre/element/html
import shared/app_state.{type AppState}

pub fn root(path: List(String), state: AppState) {
  let route = case path {
    [] -> home()
    ["quote"] -> latest_quotes(state.latest_quotes)
    ["quote", "random"] -> random_quote()
    ["quote", id_str] -> {
      let maybe_quote =
        option.from_result({
          use id <- result.try(int.parse(id_str))
          state.quotes_by_id
          |> dict.get(id)
        })

      quote_by_id(maybe_quote)
    }
    _ -> not_found()
  }

  html.div([], [
    html.nav(
      [attribute.class("sticky top-0 w-screen px-4 py-2 flex bg-[#333]")],
      [
        html.ul([attribute.class("flex gap-4 m-0 p-0")], [
          html.li([attribute.class("leading-4")], [
            link("/", [], [element.text("Home")]),
          ]),
          html.li([attribute.class("leading-4")], [
            link("/quote/random", [], [element.text("Random")]),
          ]),
          html.li([attribute.class("leading-4")], [
            link("/quote", [], [element.text("Latest")]),
          ]),
        ]),
      ],
    ),
    route,
    html.a(
      [
        attribute.class("fixed left-4 bottom-4 w-[32px] h-[29.5px]"),
        attribute.href("https://gitlab.com/kgroat/quote-explorer-gleam"),
        attribute.target("_blank"),
      ],
      [
        html.img([
          attribute.class("w-[32px] h-[29.5px]"),
          attribute.src("/static/gitlab.svg"),
          attribute.alt("GitLab Repo"),
        ]),
      ],
    ),
  ])
}
