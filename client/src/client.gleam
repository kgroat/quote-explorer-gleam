import client/view
import gleam/dict
import gleam/option.{None}
import illustrious/client.{ClientAppBuilder, build_app}
import lustre
import shared/app_state

pub fn main() {
  let app =
    build_app(ClientAppBuilder(
      view.root,
      app_state.AppState(dict.new(), None),
      app_state.updater,
      app_state.app_decoder,
    ))
  let assert Ok(_) = lustre.start(app, "#app", Nil)
}
