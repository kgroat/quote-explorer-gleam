# todo: remove explicit platform on next release
# see https://github.com/lustre-labs/dev-tools/pull/60
FROM --platform=amd64 ghcr.io/gleam-lang/gleam:v1.3.2-erlang-alpine

RUN  apk update \
  && apk add \
  watchexec \
  bash

WORKDIR /app

# download and pre-compile shared deps
RUN mkdir -p /app/shared/src
COPY ./shared/gleam.toml /app/shared
COPY ./shared/manifest.toml /app/shared
RUN  cd shared \
  && gleam deps download \
  && gleam build

# download and pre-compile client deps
RUN mkdir -p /app/client/src
COPY ./client/gleam.toml /app/client
COPY ./client/manifest.toml /app/client
RUN  cd client \
  && gleam deps download \
  && gleam build \
  && gleam run -m lustre/dev add esbuild \
  && gleam run -m lustre/dev add tailwind

# download and pre-compile server deps
RUN mkdir -p /app/server/src
COPY ./server/gleam.toml /app/server
COPY ./server/manifest.toml /app/server
RUN  cd server \
  && gleam deps download \
  && gleam build

# build client
COPY ./shared /app/shared
COPY ./client /app/client

RUN  cd client \
  && gleam run -m lustre/dev build --tailwind-entry=index.css --outdir=../server/priv/static

COPY . /app

# build server
RUN  cd server \
  && gleam build

EXPOSE 8000

HEALTHCHECK --interval=5s --timeout=30s --start-period=15s --retries=5 CMD curl http://localhost:8000 || exit 1

CMD ["./server/run.sh"]
