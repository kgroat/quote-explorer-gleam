# quote_explorer
An app to view famous quotes from throughout the ages, written entirely in [Gleam](https://gleam.run/).

[[_TOC_]]

## Quickstart
The only thing you should need to install in order to run the application is [Docker](https://docs.docker.com/get-docker/).

### Development mode
To run the app in development mode, simply run `./dev.sh` in your terminal.  This will automatically run `docker compose up`, which will:
* Download the required docker images
  * [postgres](https://hub.docker.com/_/postgres)
  * [ubuntu](https://hub.docker.com/_/ubuntu)
* Build the docker image containing both the client and server code
* Start the postgres database
* Start the app in watch mode listening on port 8000
  * When you make changes in the `server/src` directory, it will automatically restart the server
  * When you make changes in the `client/src` directory, it will automatically rebuild the assets
  * Note that both of these take time, and that you will need to refresh your browser for client changes to show up

### Production mode
To run the app in production mode, you will need to first:
* Generate a TLS certificate in PEM format and place the files in a directory called `tls` at the project root
  * The full-chain certificate should be called `cert.pem`
  * The keyfile should be called `key.pem`
* Create a `.env` file in the `server` directory
  * Look at the `.env.example` file for what to put in it

Once these pieces are in place, you can run `./prod.sh` to start the app in production mode.  This will run a similar `docker compose up` command, but will use the `docker-compose.prod.yml` file instead.  This file does most of the same things as above, but it also:
* Requires the `server/.env` file to be present
* Automatically mounts the `tls` directory as a volume
* Sets the TLS-related environment variables
* Starts the app listening for `https` connections on port 443 (not in watch mode)
* Runs in `--detach` mode, meaning the docker containers will be backgrounded
* Runs in `--wait` mode, meaning the command will wait for the services to be healthy before returning you to your terminal

To view the application application logs in production mode, you can run `docker compose logs app --follow`

## Client
The client is a SPA written in [lustre](https://github.com/lustre-labs/lustre) and compiled to the `javascript` target.

### Routing
It has a history-based router that is controlled by the `src/router.ffi.mjs` file whose functions are mounted by the `src/client/router.gleam` file.  It keeps track of the routes the user goes to, and can be controlled using the following functions:
* `push_path` will push a new path onto the `history` stack
* `replace_path` will replace the current path in the `history` stack

`src/client/router.gleam` also provides three other helper functions:
* `parse_path` takes a string path, such as `/my/cool/route` and splits it into segments, such as `["my", "cool", "route"]`
* `get_current_path` returns the current path of the app, pre-parsed
* `register_callback` registers a callback to be called whenever the user uses the back or forward buttons in their browser
  * The callback will recieve the new parsed path

The `link` function inside of `src/client/custom_elements.gleam` can be used to create an `<a>` element which automatically hooks into the routing, such that clicking it will use the history api rather than causing a full page refresh.

#### Individual routes
The actual routing to different screens is handled by `src/client/root.gleam`, which takes in the app state, and based on the parsed path within it will call the appropriate route function.  Each route function is expected to return a single lustre element.

### Application state
All application state is handled by `src/client/state.gleam`.  It defines the initial state, the effects to be run, and the way state can be mutated.  Each route may define a loader, which is expected to return a single lustre effect.  These are used by the `route_handler` in `src/client/state.gleam` to do things such as load data whenever a route is accessed.

## Server
The server can be broken down into two pieces:
* Communication with the postgres database
* The exposed REST API by which the client communicates with the server

The server is started from the `src/app.gleam` file. This file reads all of the appropriate environment variables, creates the `Context` record that is used to hold common pieces that the entire app needs access to, and starts the server.

### Database layer
Upon application startup, the `PG_URI` environment variable is expected to be present (defined for you inside the docker compose files).  The value from this variable is used to create a database connection using the `gleam_pgo` library.  This database connection is then passed down into other functions through the context record.

The database layer is governed by the files inside the `src/app/db` directory (currently only `src/app/db/quote.gleam`).  These files define the surface by which the rest of the application can communicate with the database.  Each file is responsible for handling logic for a single postgres table.

### REST layer
The REST API is governed by the `src/app/api.gleam` file and the files inside the `src/app/api` directory (currently only `src/app/api/quote.gleam`). Inside `src/app/api.gleam` you will find the `api_router` which is mounted at `/api` on the server.  Then, each route can define its own router, such as `/api/quote` routing to the `quote_router` inside of `src/app/db/quote.gleam`.  Within each sub-router, individual endpoints can be defined which respond to specific actions, such as `GET /api/quote` fetching the latest quotes.

Some APIs are secured using a preshared key, such as `POST /api/quote` and `DELETE /api/quote/[id]`.  In order to access these endpoints, you will need to supply an `Authorization` header in the form of `Bearer [key]`.  In development mode, this key is set to the value `insecure`, but in production mode you are expected to supply the key via the `QE_PRESHARED_KEY` environment variable in the `server/.env` file.
