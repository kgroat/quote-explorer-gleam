
.PHONY: start
start: server/priv/static/client.css server/priv/static/client.mjs
	cd server && gleam run

.PHONY: build
build: server/priv/static/client.css server/priv/static/client.mjs
	cd server && gleam build

server/priv/static/client.css: client/manifest.toml client/tailwind.config.js client/src/**/*.gleam client/build/**/*
	cd client && gleam run -m lustre/dev build app --tailwind-entry=index.css --outdir=../server/priv/static

server/priv/static/client.mjs: client/manifest.toml client/tailwind.config.js client/src/**/*.gleam client/build/**/*
	cd client && gleam run -m lustre/dev build app --tailwind-entry=index.css --outdir=../server/priv/static
