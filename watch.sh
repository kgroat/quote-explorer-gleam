#!/usr/bin/env bash

watchexec_main() {
  pushd server
    gleam build
  popd
  watchexec -r -w client/src -w client/index.html -w client/index.css -w client/tailwind.config.js ./client/run.sh &
  watchexec -r -w server/src ./server/run.sh
  echo "exiting..."
}

main() {
  [[ -n "$1" ]] && sleep "$1"
  if which watchexec; then
    watchexec_main
  else
    echo "You must install watchexec" >&2
    exit 1
  fi
}

main $@
